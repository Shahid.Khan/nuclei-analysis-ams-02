import numpy as np
import pandas as pd
import uproot
class TestClass(object): 
    def check(self): 
        print ("object is alive!") 
    def __del__(self): 
        print ("object deleted")    
from concurrent.futures import ThreadPoolExecutor
executor = ThreadPoolExecutor(20)
import warnings
import os
import gc
import json
import xgboost as xgb
from xgboost import XGBClassifier
from sklearn.model_selection import train_test_split
print("xgb is ",xgb.__version__)
print("uproot is ",uproot.__version__)
from datetime import date
today = date.today()

def add_data_signal_to_mc(df_mc, df_data):
    #this func was created because the previous mc model was trained on data which had two classes for each fragment there are 3 now
    dataframes = []
    label_mapping = {i: j for i, j in zip(range(32, 90, 3), range(21, 60, 2))}
    for i in range(0,90,1):
        df = df_all[df_all['label']==i]
        if i not in label_mapping:
            #because signal for charge greater than have been selected with tight manual selection and xgboost
            dataframes.extend([df])
        else:
            df = df_all[df_all['label']==i]
            df.reset_index(drop=True, inplace=True)
            df_signal = df_data[df_data['label']==label_mapping[i]]
            df_signal['label']=i
            df_signal.reset_index(drop=True, inplace=True)
            if df_signal.shape[0] >35000:
                df_signal = df_signal.sample(n=35000)
                df_red = df.drop(df.index[0:35000])
            else:
                df_red = df.drop(df.index[0:df_signal.shape[0]])
            dataframes.extend([df_signal])
            del df
            dataframes.extend([df_red])
            del df_signal, df_red
            gc.collect()
    df_tot = pd.concat(dataframes)
    return df_tot


rigidity_low = 2.15
rigidity_up  = 1200

#new lines added from here to include data signal
#signal_all = uproot.open(f"/eos/user/k/khansh/data_iss_pass8/train_data.root:t1").arrays(library='pd', 
#                                                            decompression_executor=executor, interpretation_executor=executor)
signal_all = uproot.open(f"/eos/ams/group/unige/data_only/train_data.root:t1").arrays(library='pd', 
                                                            decompression_executor=executor, interpretation_executor=executor)
gc.collect()
print("Data signal file uploaded")
#X = signal_all.iloc[:, :-1]  
#y = signal_all['label'] 
#del signal_all
#x_train, x_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=324, stratify=y)
#del X, y
#gc.collect()
#x_train = pd.concat([x_train, y_train], axis=1)
#file = uproot.recreate(f"/eos/user/k/khansh/data_iss_pass8/train_data.root", compression=uproot.ZLIB(4))
#file["t1"] = x_train
#del y_train
#gc.collect()
#x_test = pd.concat([x_test, y_test], axis=1)
#file = uproot.recreate(f"/eos/user/k/khansh/data_iss_pass8/test_data.root", compression=uproot.ZLIB(4))
#file["t1"] = x_test
#del x_test, y_test
#gc.collect()
#new lines end above

df_all = uproot.open(f"/eos/ams/group/unige/train_test/train_test_Li_to_Ni_without_chi2_89_classes_rigi_{rigidity_low}_{rigidity_up}.root:t1").arrays(library='pd', 
                                                            decompression_executor=executor, interpretation_executor=executor)
print("MC file uploaded")
n1 = 70000
df_all = df_all.groupby('label', group_keys=False).apply(lambda x: x.sample(n=n1))
df_all.reset_index(drop=True, inplace=True)
#df_all = df_all[~df_all.isin([np.nan, np.inf, -np.inf]).any(axis=1)]
#df_all.reset_index(drop=True, inplace=True)

#new lines added from here to include data signal
df = add_data_signal_to_mc(df_all, signal_all)
del df_all, signal_all
df['trd_nhitk/trd_onhitk']= df['trd_nhitk']/df['trd_onhitk']
df = df.replace([np.inf, -np.inf], np.nan)
#new lines end above

new_cuts = ['anti_nhit', 'betah2hbxy_sum', 'betah2hby_sum', 'betah2p','betah2q/tk_qin0_2', 'betah2r/tk_rigidity_0_2_2', 'nbetah',
       'ntrack', 'tk_exdis_0', 'tk_exqln_0_0_0', 'tk_exqln_0_0_1', 'tk_exqln_0_0_2', 
            'tk_exqls_0_0', 'tk_exqls_0_1', 'tk_exqls_0_2','tk_exqls_0_3', 'tk_exqls_0_4', 'tk_exqls_0_5', 'tk_exqls_0_6','tk_exqls_0_7',
            'tk_iso_0_0', 'tk_iso_0_1', 'tk_iso_1_0', 'tk_iso_1_1', 'tk_iso_2_0', 'tk_iso_2_1', 'tk_iso_3_0','tk_iso_3_1', 'tk_iso_4_0', 'tk_iso_4_1', 'tk_iso_5_0',
       'tk_iso_5_1', 'tk_iso_6_0', 'tk_iso_6_1', 'tk_iso_7_0','tk_iso_7_1',
            'tk_oel_0', 'tk_oel_1', 'tk_oel_2', 'tk_oel_3','tk_oel_4', 'tk_oel_5', 'tk_oel_6', 'tk_oel_7',
            'tk_ohitl_0','tk_ohitl_1', 'tk_ohitl_2', 'tk_ohitl_3', 'tk_ohitl_4','tk_ohitl_5', 'tk_ohitl_6', 'tk_ohitl_7',
            'tk_oq_0/tk_qin0_2', 'tk_oq_1/tk_qin0_2', 'tk_qin0_0', 'tk_qin0_1', 'tk_qin0_2',
            'tk_qln0_0_0', 'tk_qln0_0_1', 'tk_qln0_0_2', 'tk_qln0_1_0','tk_qln0_1_1', 'tk_qln0_1_2', 'tk_qln0_2_0', 'tk_qln0_2_1',
       'tk_qln0_2_2', 'tk_qln0_3_0', 'tk_qln0_3_1', 'tk_qln0_3_2', 'tk_qln0_4_0', 'tk_qln0_4_1', 'tk_qln0_4_2', 'tk_qln0_5_0',
       'tk_qln0_5_1', 'tk_qln0_5_2', 'tk_qln0_6_0', 'tk_qln0_6_1', 'tk_qln0_6_2', 'tk_qln0_7_0', 'tk_qln0_7_1', 'tk_qln0_7_2',
       'tof_oncl',  'tof_oq_0_0/tof_ql_0', 'tof_oq_0_1/tof_ql_0','tof_oq_1_0/tof_ql_1', 'tof_oq_1_1/tof_ql_1','tof_oq_2_0/tof_ql_2', 'tof_oq_2_1/tof_ql_2',
       'tof_oq_3_0/tof_ql_3', 'tof_oq_3_1/tof_ql_3', 'tof_ql_0',     'tof_ql_1', 'tof_ql_2', 'tof_ql_3', 'tk_qrmn','trd_oampk','trd_nhitk/trd_onhitk']

x = df[new_cuts].copy()
y =pd.DataFrame(df['label'], dtype='int8')
del df
gc.collect()
print("training set created")
hp_path = '/eos/ams/group/unige/model/best_hyperparameters.json'
print("hp json path is ",hp_path)
with open(f'{hp_path}', 'rb') as file:
    best_hyperparameters = json.load(file)
    
print("HPs uploaded")

param= {'alpha': best_hyperparameters['alpha'], 'gamma': best_hyperparameters['gamma'],
        'learning_rate': best_hyperparameters['learning_rate'],
        'max_depth': best_hyperparameters['max_depth'], 
        'n_estimators': best_hyperparameters['n_estimators'], 'nthread': 10,
         'objective':'multi:softmax','eval_metric': 'auc'}
xgb_clf = XGBClassifier(**param, tree_method='hist', device = "cuda")
xgb_clf.fit(x,y)
print("model trained")
xgb_clf.save_model(f"/eos/ams/group/unige/model/xgb_90_classes_without_chi2_{rigidity_low}_{rigidity_up}_{today}.model")
params = xgb_clf.get_params()

# Save the model parameters to a file
with open(f"/eos/ams/group/unige/model/xgb_90_classes_without_chi2_{rigidity_low}_{rigidity_up}_trd_variables_included_in_data_{today}_params.json", 'w') as file:
    json.dump(params, file)
print("successfully finished")
