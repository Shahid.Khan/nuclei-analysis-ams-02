# Nuclei_analysis_ams_02
This repository contains code that can convert AMS-02 root files to pandas dataframes and can apply selections on them.

To clone the repository
```
git clone https://gitlab.unige.ch/Shahid.Khan/Nuclei_analysis_ams_02.git
```
To run a pipeline on CERN HTCondor visit the directory
```
cd /path/to/Nuclei_analysis_ams_02/pipeline
condor_submit job.sh
```
The job.sh sets the number of CPUs and GPUs required. It calls the script train_labelling.sh that sets up environment and installs libraries. It then calls 4 different files, one to label the data one to create a train file, one to find HPs and one to train the model.

Once the training is performed then physics analysis can be performed using the notebook
```
https://gitlab.unige.ch/Shahid.Khan/nuclei-analysis-ams-02/-/blob/main/swan/data_analysis.ipynb
```